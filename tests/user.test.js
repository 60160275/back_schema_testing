const dbHandler = require('./db-handler')
const User = require('../models/User')

beforeaAll(async () => {
    await dbHandler.connect()
})

afterEach(async () => {
    await dbHandler.clearDatabase()
})

afterAll(async () => {
    await dbHandler.closeDatabase()
})

const userComplete = {
    name: 'Kob',
    gender: 'M'
}

describe('User', () => {
    it('สามารถเพิ่ม user ได้', async() => {
      let error = null
      try {
          const user = new User(userComplete)
          await user.save()
      } catch (e) {
          error = e
      }
      expect(error).toBeNull() 
    })
    it('ไม่สามารถเพิ่ม user ได้ เพราะ  name เป็น ช่องว่าง' ,async () => {
        let error = null
        try {
            const user = new User(userComplete)
            await user.save()
        } catch (e) {
            error = e
        }
        expect(error).not.toBeNull() 
      })
  



})

